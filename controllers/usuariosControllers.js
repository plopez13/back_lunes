const verificarToken = require("../middleware/verificarToken")


module.exports = function (app) {
    // Creamos la ruta obtener todos los productos.
   app.get("/usuarios", /*verificarToken.verificar, verificarToken.admin,*/ async function (req, res) {
      //requerimos y guardamos la ruta de services donde hara la consulta a la base
      const usuarios = require("./../services/usuariosServices");
      //hacemos la consulta y ponemos await porque no sabemos cuanto puede demorar
      //podemos seguir viendo el proceso en  clientesServices.
      const response = await usuarios.getUsuarios();
      //Una vez recibida la respuesta, se la mandamos a la ruta
      res.send(response.result);
    }),


    app.get("/usuariosId/:id", async function (req, res) {

        const id = req.params.id
   // aqui lo hacemos por id    //requerimos y guardamos la ruta de services donde hara la consulta a la base
        const usuariosId = require("./../services/usuariosServices");
        //hacemos la consulta y ponemos await porque no sabemos cuanto puede demorar
        //podemos seguir viendo el proceso en  clientesServices.
        const response = await usuariosId.getUsuariosById(id);
        //Una vez recibida la respuesta, se la mandamos a la ruta
        res.send(response.result);
      }),





      app.get("/usuariosRol/:rol", async function (req, res) {

        const rol = req.params.rol
        //requerimos y guardamos la ruta de services donde hara la consulta a la base
        const usuarios = require("./../services/usuariosServices");
        //hacemos la consulta y ponemos await porque no sabemos cuanto puede demorar
        //podemos seguir viendo el proceso en  clientesServices.
        const response = await usuarios.getUsuariosByRol(rol);
        //Una vez recibida la respuesta, se la mandamos a la ruta
        res.send(response.result);
      }),

      app.post("/usuarios", verificarToken.admin, async function (req, res) {

        const body = req.body
        //requerimos y guardamos la ruta de services donde hara la consulta a la base
        const usuarios = require("./../services/usuariosServices");
        //hacemos la consulta y ponemos await porque no sabemos cuanto puede demorar
        //podemos seguir viendo el proceso en  clientesServices.
        const response = await usuarios.postUsuario(body);
        //Una vez recibida la respuesta, se la mandamos a la ruta
        res.send(response.result);
      }),

      app.patch("/usuarios", verificarToken.admin, async function (req, res) {

        const body = req.body
        //requerimos y guardamos la ruta de services donde hara la consulta a la base
        const usuarios = require("./../services/usuariosServices");
        //hacemos la consulta y ponemos await porque no sabemos cuanto puede demorar
        //podemos seguir viendo el proceso en  clientesServices.
        const response = await usuarios.patchUsuario(body);
        //Una vez recibida la respuesta, se la mandamos a la ruta
        res.send(response.result);
      })




  }
  