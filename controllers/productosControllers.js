const verificarGet = require("./../middleware/verificarGet")
const verificarToken = require("./../middleware/verificarToken")


module.exports = function (app) {
    // Creamos la ruta obtener todos los productos.
    app.get("/productos", async function (req, res) {
      //requerimos y guardamos la ruta de services donde hara la consulta a la base
      const productos = require("./../services/productosServices");
      //hacemos la consulta y ponemos await porque no sabemos cuanto puede demorar
      //podemos seguir viendo el proceso en  clientesServices.
      const response = await productos.getProductos();
      //Una vez recibida la respuesta, se la mandamos a la ruta
      res.send(response.result);
    });
    app.get("/productos/:id", verificarGet.verificarNumero, verificarGet.verificarUno, async function (req, res) {
        const id = req.params.id
        const productos = require("./../services/productosServices");
        const response = await productos.getProductosById(id);
        res.send(response.result);
      });

  app.post("/productos", verificarToken.admin,verificarToken.verificar, async function (req, res) {
    let body = req.body
    //requerimos y guardamos la ruta de services donde hara la consulta a la base
    const productos = require("./../services/productosServices");
    //hacemos la consulta y ponemos await porque no sabemos cuanto puede demorar
    //podemos seguir viendo el proceso en  clientesServices.
    const response = await productos.PostProductos(body);
    //Una vez recibida la respuesta, se la mandamos a la ruta
    res.send(response.result);
  });

  app.put("/productos", async function (req, res) {
    let body = req.body
    //requerimos y guardamos la ruta de services donde hara la consulta a la base
    const productos = require("./../services/productosServices");
    //hacemos la consulta y ponemos await porque no sabemos cuanto puede demorar
    //podemos seguir viendo el proceso en  clientesServices.
    const response = await productos.PutProductos(body);
    //Una vez recibida la respuesta, se la mandamos a la ruta
    res.send(response.result);
  });
  app.patch("/productos", async function (req, res) {
    let body = req.body
    //requerimos y guardamos la ruta de services donde hara la consulta a la base
    const productos = require("./../services/productosServices");
    //hacemos la consulta y ponemos await porque no sabemos cuanto puede demorar
    //podemos seguir viendo el proceso en  clientesServices.
    const response = await productos.PatchProductos(body);
    //Una vez recibida la respuesta, se la mandamos a la ruta
    res.send(response.result);
  });
  };



  
  
