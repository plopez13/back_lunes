const verificarGet = require("../middleware/verificarGet");
const verificarToken = require("../middleware/verificarToken");
module.exports = function (app) {
  // Creamos la ruta obtener todos los productos.
  app.get("/rol", async function (req, res) {
    //requerimos y guardamos la ruta de services donde hara la consulta a la base
    const rol = require("./../services/rolServices");
    //hacemos la consulta y ponemos await porque no sabemos cuanto puede demorar
    //podemos seguir viendo el proceso en  clientesServices.
    const response = await rol.getRol();
    //Una vez recibida la respuesta, se la mandamos a la ruta
    res.send(response.result);
  });
  app.get(
    "/rol/:id",
    verificarGet.verificarNumero,
    verificarGet.verificarUno,
    verificarToken.verificar,
    verificarToken.admin,
    async function (req, res) {
      const id = req.params.id;
      //requerimos y guardamos la ruta de services donde hara la consulta a la base
      const rol = require("./../services/rolServices");
      //hacemos la consulta y ponemos await porque no sabemos cuanto puede demorar
      //podemos seguir viendo el proceso en  clientesServices.
      const response = await rol.getRolById(id);
      //Una vez recibida la respuesta, se la mandamos a la ruta
      res.send(response.result);
    }
  );
  app.post("/rol", async function (req, res) {
    let body = req.body;
    //requerimos y guardamos la ruta de services donde hara la consulta a la base
    const rol = require("./../services/rolServices");
    //hacemos la consulta y ponemos await porque no sabemos cuanto puede demorar
    //podemos seguir viendo el proceso en  clientesServices.
    const response = await rol.postRol(body);
    //Una vez recibida la respuesta, se la mandamos a la ruta
    res.send(response.result);
  });
  app.put("/rol", async function (req, res) {
    let body = req.body;
    //requerimos y guardamos la ruta de services donde hara la consulta a la base
    const rol = require("./../services/rolServices");
    //hacemos la consulta y ponemos await porque no sabemos cuanto puede demorar
    //podemos seguir viendo el proceso en  clientesServices.
    const response = await rol.putRol(body);
    //Una vez recibida la respuesta, se la mandamos a la ruta
    res.send(response.result);
  });
  app.patch("/rol", async function (req, res) {
    let body = req.body;
    //requerimos y guardamos la ruta de services donde hara la consulta a la base
    const rol = require("./../services/rolServices");
    //hacemos la consulta y ponemos await porque no sabemos cuanto puede demorar
    //podemos seguir viendo el proceso en  clientesServices.
    const response = await rol.patchRol(body);
    //Una vez recibida la respuesta, se la mandamos a la ruta
    res.send(response.result);
  });
};
