const mysql = require("mysql");
//requerimos el archivo donde tenemos configurada la conexion
const conn = require("../config/conn");

//creamos la constante a ser exportada

const usuarios = {
    //dentro de ella ponemos una funcion asincrona, porque no sabemos cuanto demora la base en responder
    async getUsuariosById(id) {
      //Guardamos en una variable la consulta que queremos generar
      let sql = "SELECT * FROM usuarios WHERE id =" + id; //Con el archivo de conexion a la base, enviamos la consulta a la misma //Ponemos un await porque desconocemos la demora de la misma
      let resultado = await conn.query(sql);
      let response = { error: "No se encontraron registros" };
      if (resultado.code) {
        response = { error: "Error en la consulta SQL" };
      } else if (resultado.length > 0) {
        response = { result: resultado };
      }
      return response;
    },

    async postUsuario(body) {
      //Guardamos en una variable la consulta que queremos generar
      let sql = "INSERT INTO `usuarios`(`id`, `nombre`, `email`, `password`, `rol`) VALUES (NULL,'" + body.nombre +"','" + body.email + "','" + body.password + "','" + body.rol + "')"
      console.log(body)
      
      let resultado = await conn.query(sql);
      let response = { error: "Se pudo crear registros" };
      if (resultado.code) {
        response = { error: "Error en la consulta SQL" };
      } else if (resultado.length > 0) {
        response = { result: resultado };
      }
      return response;
    },
  async postUsuario(body) {
    //Guardamos en una variable la consulta que queremos generar
    let sql =
      "INSERT INTO `usuarios`(`id`, `nombre`, `email`, `password`, `rol`) VALUES (NULL,'" +
      body.nombre +
      "','" +
      body.email +
      "','" +
      body.password +
      "','" +
      body.rol +
      "')";
    console.log(body);

    let resultado = await conn.query(sql);
    let response = { error: " se pudo crear registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },

    async patchUsuario(body) {
      //Guardamos en una variable la consulta que queremos generar
      let sql = "UPDATE `usuarios` SET `nombre`='"+body.nombre+"' WHERE `id` = '"+body.id +"'"
      console.log(body)
      
      let resultado = await conn.query(sql);
      let response = { error: "No se pudo modificar registros" };
      if (resultado.code) {
        response = { error: "Error en la consulta SQL" };
      } else if (resultado.length > 0) {
        response = { result: resultado };
      }
      return response;
    },





  //dentro de ella ponemos una funcion asincrona, porque no sabemos cuanto demora la base en responder
  async getUsuariosById(id) {
    //Guardamos en una variable la consulta que queremos generar
    let sql = "SELECT * FROM usuarios WHERE id =" + id; //Con el archivo de conexion a la base, enviamos la consulta a la misma //Ponemos un await porque desconocemos la demora de la misma
    let resultado = await conn.query(sql);
    let response = { error: "No se encontraron usuarios con el rol" + rol };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },

  async postUsuario(body) {
    //Guardamos en una variable la consulta que queremos generar
    let sql =
      "INSERT INTO `usuarios`(`id`, `nombre`, `email`, `password`, `rol`) VALUES (NULL,'" +
      body.nombre +
      "','" +
      body.email +
      "','" +
      body.password +
      "','" +
      body.rol +
      "')";
    console.log(body);

    let resultado = await conn.query(sql);
    let response = { error: " se pudo crear registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },

  async patchUsuario(body) {
    //Guardamos en una variable la consulta que queremos generar
    let sql =
      "UPDATE `usuarios` SET `nombre`='" +
      body.nombre +
      "' WHERE `id` = '" +
      body.id +
      "'";
    console.log(body);

    let resultado = await conn.query(sql);
    let response = { error: "No se pudo modificar registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },

  //dentro de ella ponemos una funcion asincrona, porque no sabemos cuanto demora la base en responder
  async getUsuariosByRol(rol) {
    //Guardamos en una variable la consulta que queremos generar
    let sql = "SELECT * FROM usuarios WHERE rol =" + rol; //Con el archivo de conexion a la base, enviamos la consulta a la misma //Ponemos un await porque desconocemos la demora de la misma
    let resultado = await conn.query(sql);
    let response = { error: "No se encontraron usuarios con el rol " + rol };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },

  async getUsuarios() {
    //Guardamos en una variable la consulta que queremos generar
    let sql = "SELECT * FROM usuarios WHERE 1"; //Con el archivo de conexion a la base, enviamos la consulta a la misma //Ponemos un await porque desconocemos la demora de la misma
    let resultado = await conn.query(sql);
    let response = { error: "No se encontraron registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },
};

module.exports = usuarios;
