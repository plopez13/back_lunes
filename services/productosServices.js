const mysql = require("mysql");
//requerimos el archivo donde tenemos configurada la conexion
const conn = require("../config/conn");

//creamos la constante a ser exportada
const productos = {
  //dentro de ella ponemos una funcion asincrona, porque no sabemos cuanto demora la base en responder
  async getProductos() {
    //Guardamos en una variable la consulta que queremos generar
    let sql = "SELECT * FROM productos"; //Con el archivo de conexion a la base, enviamos la consulta a la misma //Ponemos un await porque desconocemos la demora de la misma
    let resultado = await conn.query(sql);
    let response = { error: "No se encontraron registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },
  async PostProductos (body) {

    
    let sql =  "SELECT * FROM productos";
     "INSERT INTO 'productos' ('id', 'nombre', 'email', 'direccion', 'telefono') VALUES (NULL, '" + body.nombre + body.email +"', '[value-4]', '[value-5]')"

    let resultado = await conn.query(sql);
    console.log(sql)
    let response = { error: "Se encontraron registros" };
    console.log (response)
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },
    
  async PutProductos (body) {

        console.log ("______________iiiiiiiiiiiiiiiiiiiiiiiii")
    console.log (body)
    let sql = "UPDATE `productos` SET `nombre`='" + body.nombre + "',`precio`='" + body.precio + "',`stock`='" + body.stock + "',`categoria_id`='" + body.categoria + "' WHERE id = " + body.id
    let resultado = await conn.query(sql);

    console.log(sql)
    let response = { error: "No se modificaron registros" };
    console.log (response)
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },

  async PatchProductos (body) {

    console.log ("______________iiiiiiiiiiiiiiiiiiiiiiiii")
console.log (body)
let sql = "UPDATE `productos` SET `nombre`='" + body.nombre + "',`precio`='" + body.precio + "',`stock`='" + body.stock + "',`categoria_id`='" + body.categoria + "' WHERE id = " + body.id
let resultado = await conn.query(sql);
console.log(sql)
let response = { error: "No se modificaron registros" };
console.log (response)
if (resultado.code) {
  response = { error: "Error en la consulta SQL" };
} else if (resultado.length > 0) {
  response = { result: resultado };
}
return response;
},


  async getProductosById(id) {
    
    let sql = "SELECT * FROM productos WHERE id =" + id;
    let resultado = await conn.query(sql);
    console.log ("______________")
    console.log(sql)
    let response = { error: "No se encontraron registros" };
    console.log (response)
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },

};
module.exports = productos;