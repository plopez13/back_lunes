
const verificarToken = require("../middleware/verificarToken");



module.exports = function (app) {
  // Creamos la ruta obtener todos los productos.
  app.get("/clientes/:id/:otra", async function (req, res) {
    let id = req.params.id;
    let otra = req.params.otra;
    //requerimos y guardamos la ruta de services donde hara la consulta a la base
    const clientes = require("./../services/clientesServices");
    //hacemos la consulta y ponemos await porque no sabemos cuanto puede demorar
    //podemos seguir viendo el proceso en  clientesServices.
    const response = await clientes.getClientes(id, otra);
    //Una vez recibida la respuesta, se la mandamos a la ruta
    res.send(response.result);
  });
  app.post("/clientes", async function (req, res) {
    let body = req.body;
    //requerimos y guardamos la ruta de services donde hara la consulta a la base
    const clientes = require("./../services/clientesServices");
    //hacemos la consulta y ponemos await porque no sabemos cuanto puede demorar
    //podemos seguir viendo el proceso en  clientesServices.
    const response = await clientes.PostClientes(body);
    //Una vez recibida la respuesta, se la mandamos a la ruta
    res.send(response.result);
  });


app.post("/clientes", verificarToken.admin,verificarToken.verificar, async function (req, res) {
  let body = req.body
  //requerimos y guardamos la ruta de services donde hara la consulta a la base
  const clientes = require("./../services/clientesServices");
  //hacemos la consulta y ponemos await porque no sabemos cuanto puede demorar
  //podemos seguir viendo el proceso en  clientesServices.
  const response = await clientes.PostClientes(body);
  //Una vez recibida la respuesta, se la mandamos a la ruta
  res.send(response.result);
});
};
