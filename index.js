const express = require("express");
const app = express();

const bodyParser = require("body-parser");
const cors = require("cors");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

process.env.SECRET = process.env.SECRET || 'lejtnoqhfrnzdoupbdwintdlhzhelisb';
const secret = process.env.SECRET;


app.use(cors());

/* app.get("/usuarios/:id", async function (req, res) {
  let id = req.params.id;
  res.send(id);
});
app.get("/otro", async function (req, res) {
  res.send("Algo se");
}); */


require("./controllers/clientesControllers")(app);
require("./controllers/productosControllers")(app);
require("./controllers/loginControllers")(app);
require("./controllers/ventasController")(app);


app.listen(3000, function () {
  console.log("Servidor iniciado en el puerto 3000");
});
/* app.post("/otro", async function (req, res) {
  let body = req.body;
  res.send(body.probando);
}); */

