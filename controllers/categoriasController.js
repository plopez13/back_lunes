const verificarGet = require("../middleware/verificarGet");
const verificarToken = require("../middleware/verificarToken")

module.exports = function (app) {
    // Creamos la ruta obtener todos los productos.
    app.get("/categorias", async function (req, res) {
      //requerimos y guardamos la ruta de services donde hara la consulta a la base
      const categorias = require("./../services/categoriasServices");
      //hacemos la consulta y ponemos await porque no sabemos cuanto puede demorar
      //podemos seguir viendo el proceso en  clientesServices.
      const response = await categorias.getCategorias();
      //Una vez recibida la respuesta, se la mandamos a la ruta
      res.send(response.result);
    });
    app.get("/categorias/:id",verificarGet.verificarNumero,verificarToken.admin,verificarToken.verificar,async function (req, res) {
        const id = req.params.id
        const categorias = require("./../services/categoriasServices");
        const response = await categorias.getCategoriasById(id);
        res.send(response.result);
      });
      app.get("/categorias/:id/:name",async function (req, res) {
        const id = req.params.id
        const name = req.params.name
        const categorias = require("./../services/categoriasServices");
        const response = await categorias.getCategoriasById(id,name);
        res.send(response.result);
      });
      app.post("/categorias",async function (req, res) {
        const body = req.body
        const categorias = require("./../services/categoriasServices");
        const response = await categorias.postCategorias(body);       
        res.send(response.result);
      });
      app.put("/categorias",async function (req, res) {
        const body = req.body
        const categorias = require("./../services/categoriasServices");
        const response = await categorias.putCategorias(body);       
        res.send(response.result);
      });
      app.patch("/categorias",async function (req, res) {
        const body = req.body
        const categorias = require("./../services/categoriasServices");
        const response = await categorias.patchCategorias(body);       
        res.send(response.result);
      });
    };

  