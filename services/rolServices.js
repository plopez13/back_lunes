const mysql = require("mysql");

const conn = require("../config/conn");

const rol = {
  async getRol() {
    let sql = "SELECT * FROM rol";
    let resultado = await conn.query(sql);
    let response = { error: "No se encontraron registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },
  async getRolById(id) {
    let sql = "SELECT * FROM rol WHERE id = " + id;
    let resultado = await conn.query(sql);
    let response = { error: "No se encontraron registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },
  async postRol(body) {
    console.log(body);
    let sql =
      "INSERT INTO `rol`(`id`, `nombre`) VALUES (NULL,' " + body.nombre + "');";
    console.log(sql);

    let resultado = await conn.query(sql);
    let response = { error: "No se encontraron registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },
  async putRol(body) {
    console.log(body);
    let sql =
      " UPDATE `rol` SET `nombre`='" + body.nombre + "' WHERE id=" + body.id;
    console.log(sql);
    let resultado = await conn.query(sql);
    let response = { error: "No se encontraron registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },
  async patchRol(body) {
    console.log(body);
    let sql =
      " UPDATE `rol` SET `nombre`='" + body.nombre + "' WHERE id=" + body.id;
    console.log(sql);
    let resultado = await conn.query(sql);
    let response = { error: "No se encontraron registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },
};

module.exports = rol;

/* const mysql = require("mysql");
//requerimos el archivo donde tenemos configurada la conexion
const conn = require("../config/conn");

//creamos la constante a ser exportada
const rol = {
  //dentro de ella ponemos una funcion asincrona, porque no sabemos cuanto demora la base en responder
  async getRol() {
    //Guardamos en una variable la consulta que queremos generar
    let sql = "SELECT * FROM rol"; //Con el archivo de conexion a la base, enviamos la consulta a la misma //Ponemos un await porque desconocemos la demora de la misma
    let resultado = await conn.query(sql);
    let response = { error: "No se encontraron registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },
  async getRolById(id) {
    //Guardamos en una variable la consulta que queremos generar
    let sql = "SELECT * FROM rol WHERE id = " + id; //Con el archivo de conexion a la base, enviamos la consulta a la misma //Ponemos un await porque desconocemos la demora de la misma
    let resultado = await conn.query(sql);
    let response = { error: "No se encontraron registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },
  async postRol(body) {
    //Guardamos en una variable la consulta que queremos generar
    console.log(body);
    let sql =
      "INSERT INTO `rol`(`id`, `nombre`) VALUES (NULL,' " + body.nombre + "');";
    console.log(sql);
    //Con el archivo de conexion a la base, enviamos la consulta a la misma //Ponemos un await porque desconocemos la demora de la misma
    let resultado = await conn.query(sql);
    let response = { error: "No se encontraron registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },
};

module.exports = rol;
 */
